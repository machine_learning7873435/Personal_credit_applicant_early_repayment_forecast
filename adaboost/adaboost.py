# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 11:17:32 2023

@author: asus
"""
# Boosting

#%%  导入程序包和函数
import numpy as np
import pandas as pd
from sklearn import preprocessing
# 2. 绘图函数
import matplotlib.pyplot as plt
# 3. 样本划分，CV交叉验证
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
# 4. 插值法
from sklearn.impute import SimpleImputer
# 5. 决策树、Bagging、Boosting、Random Forest
from sklearn.tree import DecisionTreeClassifier, plot_tree,DecisionTreeRegressor
from sklearn.ensemble import BaggingClassifier,BaggingRegressor
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
# 6. ROC,AUC
from sklearn.metrics import plot_roc_curve
from sklearn.metrics import roc_auc_score

#%% 建立模型
# 1. 导入数据
X = pd.read_excel(r'C:\Users\asus\Desktop\final examination\Data.xlsx')
X.info()

# 2. 数据清洗
flotcols = X.select_dtypes(include='float64').columns
imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
X[flotcols] = pd.DataFrame(imputer.fit_transform(X[flotcols]))
for i in range(len(X)):
    if X.at[i,'jieju_loan_usage_desc'] =='个人贷款用途':
        X.at[i,'jieju_loan_usage_desc'] = 1
    elif X.at[i,'jieju_loan_usage_desc'] =='借新还旧':
        X.at[i,'jieju_loan_usage_desc'] = 2
    else:
        X.at[i,'jieju_loan_usage_desc'] = 3
X.drop(['jieju_dubil_bal','jieju_mbank_prin','jieju_asset_flow_trans_bal','id'],axis=1,inplace=True)
corr=X.corr()
X.drop(['jieju_exec_int_rate_val'],axis=1,inplace=True)

# 3. 选取特征变量与响应变量
X1=pd.DataFrame()
for i in range(len(X)):
    X1['jieju_dubil_amt'] = X['jieju_dubil_amt']
    X1['jieju_h_max_ovdue_days'] = X['jieju_h_max_ovdue_days']
    X1['jieju_loan_usage_desc'] = X['jieju_loan_usage_desc']
    X1['jieju_punish_int_exec_int_rate_val'] = X['jieju_punish_int_exec_int_rate_val']
    X1['jieju_obank_contri_amt'] = X['jieju_obank_contri_amt']
    X1['kehu_age'] = X['kehu_age']
    X1['kehu_yr_incom_amt'] = X['kehu_yr_incom_amt']
    X1['shouxin_crdt_limit'] = X['shouxin_crdt_limit']
    X1['shouxin_aval_limit'] = X['shouxin_aval_limit']
    X1['shouxin_used_limit'] = X['shouxin_used_limit']

# 响应变量
y = X['Y']

# 4. 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X1, y, stratify=y, test_size=0.2, random_state=123)
# stratify=y:测试集与整个数据集里y的数据分类比例一致 

#%% 建立模型
# 1.初始模型
model = AdaBoostClassifier(random_state=123)
model.fit(X_train, y_train)
model.score(X_test, y_test)
# 1.1. 绘制ROC曲线
plot_roc_curve(model, X_test, y_test)
x = np.linspace(0, 1, 100)
plt.plot(x, x, 'k--', linewidth=1)
plt.title('ROC Curve for Adaboost')
plt.savefig("Adaboost.png",dpi=400)
# 1.2. 计算AUC值
prob = model.predict_proba(X_test)
roc_auc_score(y_test,prob[:,1])

# 2. K折交叉验证寻找最优超参数
# 2.1. 交叉验证得到最优超参数
param_grid = {'n_estimators': [90,100],'learning_rate': [0.8,0.9,1]}
kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=123)
model_cv = GridSearchCV(AdaBoostClassifier(random_state=123), param_grid,cv=kfold)
model_cv.fit(X_train,y_train) 

# 2.2. 最佳超参数下的模型
model_cv.best_params_
model_cv.best_estimator_
#返回最优超参数取值
model_boosting = model_cv.best_estimator_
#返回最终模型预测结果

# 2.3. 分类器的准确率
model_boosting.score(X_test,y_test)

# 3. 绘制最终ROC曲线并计算AUC值
# 3.1. 绘制ROC曲线
plot_roc_curve(model_boosting, X_test, y_test)
x = np.linspace(0, 1, 100)
plt.plot(x, x, 'k--', linewidth=1)
plt.title('ROC Curve for Adaboost_CV')
plt.savefig("Adaboost_CV.png",dpi=400)
# 3.2. 计算AUC值
prob_boosting = model_boosting.predict_proba(X_test)
roc_auc_score(y_test,prob_boosting[:,1])



