# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 11:17:32 2023

@author: asus
"""
#%%  导入程序包和函数
# 1. 数据分析函数
import numpy as np
import pandas as pd
from sklearn import preprocessing
# 2. 绘图函数
import matplotlib.pyplot as plt
# 3. 样本划分，CV交叉验证
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
# 4. 插值法
from sklearn.impute import SimpleImputer
# 5. 决策树、Bagging、Boosting、Random Forest
from sklearn.tree import DecisionTreeClassifier, plot_tree,DecisionTreeRegressor
from sklearn.ensemble import BaggingClassifier,BaggingRegressor
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
# 6. ROC,AUC
from sklearn.metrics import plot_roc_curve
from sklearn.metrics import roc_auc_score

#%% 数据分析
# 1. 导入数据
X = pd.read_excel(r'C:\Users\asus\Desktop\final examination\Data.xlsx')
X.info()

# 2. 数据清洗
flotcols = X.select_dtypes(include='float64').columns
imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
X[flotcols] = pd.DataFrame(imputer.fit_transform(X[flotcols]))
for i in range(len(X)):
    if X.at[i,'jieju_loan_usage_desc'] =='个人贷款用途':
        X.at[i,'jieju_loan_usage_desc'] = 1
    elif X.at[i,'jieju_loan_usage_desc'] =='借新还旧':
        X.at[i,'jieju_loan_usage_desc'] = 2
    else:
        X.at[i,'jieju_loan_usage_desc'] = 3
X.drop(['jieju_dubil_bal','jieju_mbank_prin','jieju_asset_flow_trans_bal','id'],axis=1,inplace=True)
corr=X.corr()
X.drop(['jieju_exec_int_rate_val'],axis=1,inplace=True)

# 3. 选取特征变量与响应变量
X1=pd.DataFrame()
for i in range(len(X)):
    X1['jieju_dubil_amt'] = X['jieju_dubil_amt']
    X1['jieju_h_max_ovdue_days'] = X['jieju_h_max_ovdue_days']
    X1['jieju_loan_usage_desc'] = X['jieju_loan_usage_desc']
    X1['jieju_punish_int_exec_int_rate_val'] = X['jieju_punish_int_exec_int_rate_val']
    X1['jieju_obank_contri_amt'] = X['jieju_obank_contri_amt']
    X1['kehu_age'] = X['kehu_age']
    X1['kehu_yr_incom_amt'] = X['kehu_yr_incom_amt']
    X1['shouxin_crdt_limit'] = X['shouxin_crdt_limit']
    X1['shouxin_aval_limit'] = X['shouxin_aval_limit']
    X1['shouxin_used_limit'] = X['shouxin_used_limit']

# 响应变量
y = X['Y']

# 4. 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X1, y, stratify=y, test_size=0.2, random_state=123)
# stratify=y:测试集与整个数据集里y的数据分类比例一致 

#%% 建立模型
# 1. 初始模型
model = DecisionTreeClassifier(max_depth=3,random_state=123) #默认是gini值生成决策树
model.fit(X_train, y_train) 
model.predict(X_test)
model.score(X_train,y_train)

# 1.1. 绘制ROC曲线
plot_roc_curve(model, X_test, y_test)
x = np.linspace(0, 1, 100)
plt.plot(x, x, 'k--', linewidth=1)
plt.title('ROC Curve for DecisionTree')
plt.savefig("DecisionTree.png",dpi=400)
# 1.2. 计算AUC值
prob_b = model.predict_proba(X_test)
roc_auc_score(y_test,prob_b[:,1])


# 2. K折交叉验证寻找最优超参数
# 成本复杂性剪枝---为了提高模型的泛化能力，防止过拟合
path = model.cost_complexity_pruning_path(X_train, y_train)

# 2.1 K折交叉验证寻找最优超参数
param_grid = {'ccp_alpha': path.ccp_alphas}
kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=123)
#stratifiedfold类似于kfold，但是它是分层采样，确保训练集，测试集中各类别样本的比例与原始数据集中相同。
#shuffle=true:打乱顺序；random_state若设置值了则shuffle必须为true
model_cv = GridSearchCV(DecisionTreeClassifier(random_state=123), param_grid, cv=kfold)
model_cv.fit(X_train, y_train)  

# 2.2. 最佳超参数下的模型
model_cv.best_params_
#返回最优超参数取值
model_dt = model_cv.best_estimator_
#返回最终模型预测结果

# 2.3. 分类器的准确率
model_dt.score(X_test,y_test)

# 3. 绘制决策树
plot_tree(model_dt, feature_names=X_train.columns, impurity=True, proportion=True,rounded=True, precision=2)
plt.savefig("Decision Tree.png",dpi=400)

# 4. 绘制最终ROC曲线并计算AUC值
# 4.1 绘制ROC曲线
plot_roc_curve(model_dt, X_test, y_test)
x = np.linspace(0, 1, 100)
plt.plot(x, x, 'k--', linewidth=1)
plt.title('ROC Curve for DecisionTree_CV')
plt.savefig("ROC DecisionTree_CV.png",dpi=400)

# 4.2. 计算AUC值
prob = model_dt.predict_proba(X_test)
roc_auc_score(y_test,prob[:,1])

# 5. 变量重要性
model_dt.feature_importances_
#将特征重要性按照从小到大排序，返回index
sorted_index = model_dt.feature_importances_.argsort()
plt.barh(range(X_train.shape[1]), model_dt.feature_importances_[sorted_index])
plt.yticks(np.arange(X_train.shape[1]), X_train.columns[sorted_index])
plt.xlabel('Feature Importance')
plt.ylabel('Feature')
plt.title('DecisionTree_FI')
plt.savefig("DecisionTree_FI.png",dpi=400)
