# -*- coding: utf-8 -*-
"""
Created on Fri Apr 28 13:01:55 2023

@author: asus
"""
# 随机森林

#%% 导入程序包和函数
# 1. 数据分析函数
import numpy as np
import pandas as pd
# 2. 绘图函数
import matplotlib.pyplot as plt
# 3. 样本划分，CV交叉验证
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
# 4. 插值法
from sklearn.impute import SimpleImputer
# 5. Random Forest
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
# 6. # 6. ROC,AUC
from sklearn.metrics import plot_roc_curve
from sklearn.metrics import roc_auc_score

#%%  建立模型
# 1. 导入数据
X = pd.read_excel(r'C:\Users\asus\Desktop\final examination\Data.xlsx')
X.info()

# 2. 数据清洗
flotcols = X.select_dtypes(include='float64').columns
imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
X[flotcols] = pd.DataFrame(imputer.fit_transform(X[flotcols]))
for i in range(len(X)):
    if X.at[i,'jieju_loan_usage_desc'] =='个人贷款用途':
        X.at[i,'jieju_loan_usage_desc'] = 1
    elif X.at[i,'jieju_loan_usage_desc'] =='借新还旧':
        X.at[i,'jieju_loan_usage_desc'] = 2
    else:
        X.at[i,'jieju_loan_usage_desc'] = 3
X.drop(['jieju_dubil_bal','jieju_mbank_prin','jieju_asset_flow_trans_bal','id'],axis=1,inplace=True)
corr=X.corr()
X.drop(['jieju_exec_int_rate_val'],axis=1,inplace=True)

# 3. 选取特征变量与响应变量
X1=pd.DataFrame()
for i in range(len(X)):
    X1['jieju_dubil_amt'] = X['jieju_dubil_amt']
    X1['jieju_h_max_ovdue_days'] = X['jieju_h_max_ovdue_days']
    X1['jieju_loan_usage_desc'] = X['jieju_loan_usage_desc']
    X1['jieju_punish_int_exec_int_rate_val'] = X['jieju_punish_int_exec_int_rate_val']
    X1['jieju_obank_contri_amt'] = X['jieju_obank_contri_amt']
    X1['kehu_age'] = X['kehu_age']
    X1['kehu_yr_incom_amt'] = X['kehu_yr_incom_amt']
    X1['shouxin_crdt_limit'] = X['shouxin_crdt_limit']
    X1['shouxin_aval_limit'] = X['shouxin_aval_limit']
    X1['shouxin_used_limit'] = X['shouxin_used_limit']

# 响应变量
y = X['Y']

# 4. 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X1, y, stratify=y, test_size=0.2, random_state=123)
# stratify=y:测试集与整个数据集里y的数据分类比例一致 

# 3.1. 初始模型
model_rf = RandomForestClassifier(n_estimators=500, max_features='sqrt',random_state=123)
model_rf.fit(X_train, y_train)
model_rf.score(X_test, y_test)
# 3.2. 绘制roc曲线，计算初始auc
#  绘制roc曲线
plot_roc_curve(model_rf, X_test, y_test)
x = np.linspace(0, 1, 100)
plt.plot(x, x, 'k--', linewidth=1)
plt.title('ROC Curve for RF')
plt.savefig("RF.png",dpi=400)
# 计算初始auc
prob_rf = model_rf.predict_proba(X_test)
roc_auc_score(y_test,prob_rf[:,1])
 
# 3.3. 交叉验证得到最优参数
param_grid = {'n_estimators': [500,600,700],'max_features':[1,'sqrt',7]}
kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=123)
model_rfcv = GridSearchCV(RandomForestClassifier(random_state=123), param_grid,cv=kfold)
model_rfcv.fit(X_train, y_train)     
# 3.4. 最佳参数下的模型
model_rfcv.best_params_
model_rfcv1 = model_rfcv.best_estimator_

# 3.5. 分类器的准确率
model_rfcv1.score(X_test,y_test)
# 3.6. 绘制roc曲线
plot_roc_curve(model_rfcv1, X_test, y_test)
x = np.linspace(0, 1, 100)
plt.plot(x, x, 'k--', linewidth=1)
plt.title('ROC Curve for RandomForest')
plt.savefig("RF_cv.png",dpi=400)
# 3.7. 计算auc
prob_rfcv = model_rfcv1.predict_proba(X_test)
roc_auc_score(y_test,prob_rfcv[:,1])
